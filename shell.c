#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <dirent.h>


char bufsalida[] = {"exit"};
char dir[] = {"cd"};

parse(buf, args)
char *buf;
char **args;
{
	while(*buf != NULL)
	{
		while((*buf == ' ') || (*buf == '\t'))
			*buf++ = NULL;	

		*args++ = buf;

		while((*buf != NULL ) && (*buf != ' ' ) && (*buf != '\t'))
			buf++;
	}
	*args = NULL;
}

execute(char **args)
{
	int pid, status;
	DIR *dir;
	
	if((pid = fork()) < 0)
	{
		perror("Fork");
		exit(1);
	}
	if(pid == 0)
	{
		execvp(*args, args);
		//printf("args= %s", args);
		if(strcmp(*args,bufsalida)==0)
			printf("Bye\n");
		else
			perror(*args);
		exit(1);
	}
	while(wait(&status) != pid)
	;	
}

main()
{
	char buf[1024];
	char *args[64];

	int ppid = getpid();

	char host[1024] = "\0";
	char *user;
	char *pwd;

	gethostname(host,1023); // = getenv("LOGNAME");
	user = getenv("USERNAME");
	pwd = getenv("PWD");

	
	//for(;;)
	do{
		printf("[%s@%s]:%s->",host, user, pwd);
		
		if(gets(buf) == NULL)
		{
			printf("\n");
			exit(0);
		}

		parse(buf, args);
		execute(args);
		
	}while(strcmp(buf, bufsalida) != 0);
}



